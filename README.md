# v4l2_cam_ros

#### 介绍
v4l2读lvds相机，转rostopic

#### 软件架构
利用v4l2框架读取相机数据，
读取出的数据为RGB24格式，（具体相机不同，一般yuv或mjpg较多）
然后赋值给OpenCV的Mat数据，
再通过ROS以话题的形式发布出来。

#### 安装教程

创建ros工作空间，丢到src中catkin_make

#### 使用说明

1. 正确获得相机设备号，本例中为/dev/video2
2. 各相机支持输出的图像形式不同，本例为RGB24
3. 此代码绝大部分参考了 <http://blog.csdn.net/c406495762/ > 以及yavta.c 
特此致谢！
4. 更新说明：增加了launch文件，修改设备号为/dev/video0，并且这个设备号读到的图像为4个1280*720分辨率的图像拼接而成。然后将其中一路视为左目，其中一路视为右目，增加了相机camera_info的输出。主要参考了ros的usb_cam